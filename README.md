<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_google"></a> [google](#requirement\_google) | ~> 4.59.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | ~> 4.59.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_name"></a> [name](#module\_name) | git::https://gitlab.com/terraform-modules1582413/name.git | n/a |

## Resources

| Name | Type |
|------|------|
| [google_redis_instance.main](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/redis_instance) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_gcp_network_id"></a> [gcp\_network\_id](#input\_gcp\_network\_id) | n/a | `string` | n/a | yes |
| <a name="input_gcp_network_project_id"></a> [gcp\_network\_project\_id](#input\_gcp\_network\_project\_id) | n/a | `string` | n/a | yes |
| <a name="input_gcp_project_id"></a> [gcp\_project\_id](#input\_gcp\_project\_id) | n/a | `string` | n/a | yes |
| <a name="input_gcp_region"></a> [gcp\_region](#input\_gcp\_region) | n/a | `string` | n/a | yes |
| <a name="input_gcp_zone"></a> [gcp\_zone](#input\_gcp\_zone) | n/a | `string` | n/a | yes |
| <a name="input_gcp_zone_alternative"></a> [gcp\_zone\_alternative](#input\_gcp\_zone\_alternative) | n/a | `string` | n/a | yes |
| <a name="input_project_environment"></a> [project\_environment](#input\_project\_environment) | n/a | `string` | n/a | yes |
| <a name="input_project_name"></a> [project\_name](#input\_project\_name) | n/a | `string` | n/a | yes |
| <a name="input_project_prefix"></a> [project\_prefix](#input\_project\_prefix) | n/a | `string` | n/a | yes |
| <a name="input_redis_custom_name"></a> [redis\_custom\_name](#input\_redis\_custom\_name) | n/a | `string` | `"redis"` | no |
| <a name="input_redis_maintenance_window_day"></a> [redis\_maintenance\_window\_day](#input\_redis\_maintenance\_window\_day) | n/a | `string` | `"WEDNESDAY"` | no |
| <a name="input_redis_maintenance_window_hours"></a> [redis\_maintenance\_window\_hours](#input\_redis\_maintenance\_window\_hours) | n/a | `number` | `3` | no |
| <a name="input_redis_maintenance_window_minutes"></a> [redis\_maintenance\_window\_minutes](#input\_redis\_maintenance\_window\_minutes) | n/a | `number` | `0` | no |
| <a name="input_redis_memory_size_gb"></a> [redis\_memory\_size\_gb](#input\_redis\_memory\_size\_gb) | n/a | `number` | `1` | no |
| <a name="input_redis_tier"></a> [redis\_tier](#input\_redis\_tier) | n/a | `string` | `"BASIC"` | no |
| <a name="input_redis_version"></a> [redis\_version](#input\_redis\_version) | n/a | `string` | `"REDIS_6_X"` | no |

## Outputs

No outputs.
<!-- END_TF_DOCS -->