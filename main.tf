resource "google_redis_instance" "main" {
  project        = var.gcp_project_id
  region         = var.gcp_region
  name           = module.name.full
  tier           = var.redis_tier
  memory_size_gb = var.redis_memory_size_gb

  location_id = var.gcp_zone

  authorized_network = var.gcp_network_id
  connect_mode       = "PRIVATE_SERVICE_ACCESS"

  redis_version = var.redis_version
  display_name  = module.name.full

  redis_configs = {
    maxmemory-policy = var.redis_maxmemory_policy
  }

  labels = {
    "environment" = var.project_environment
    "project"     = var.project_name
    "name"        = var.redis_custom_name
  }

  maintenance_policy {
    weekly_maintenance_window {
      day = var.redis_maintenance_window_day
      start_time {
        hours   = var.redis_maintenance_window_hours
        minutes = var.redis_maintenance_window_minutes
        seconds = 0
        nanos   = 0
      }
    }
  }
}
