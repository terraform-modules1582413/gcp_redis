variable "project_prefix" {
  type = string
}
variable "project_environment" {
  type = string
}
variable "project_name" {
  type = string
}

variable "gcp_project_id" {
  type = string
}
variable "gcp_network_project_id" {
  type = string
}
variable "gcp_region" {
  type = string
}
variable "gcp_zone" {
  type = string
}
variable "gcp_network_id" {
  type = string
}

variable "redis_custom_name" {
  type    = string
  default = "redis"
}
variable "redis_version" {
  type    = string
  default = "REDIS_6_X"
}
variable "redis_tier" {
  type    = string
  default = "BASIC"
}
variable "redis_memory_size_gb" {
  type    = number
  default = 1
}
variable "redis_maxmemory_policy" {
  type    = string
  default = "allkeys-lfu"
}
variable "redis_maintenance_window_day" {
  type    = string
  default = "WEDNESDAY"
}
variable "redis_maintenance_window_hours" {
  type    = number
  default = 3
}
variable "redis_maintenance_window_minutes" {
  type    = number
  default = 0
}